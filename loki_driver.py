import requests, time, json, datetime, socket, sys

class driver:
  
  password = user = ""
  labels = {}
  level = host = filename = python_version = False
  push_endpoint = "/loki/api/v1/push"
  query_endpoint = "/loki/api/v1/query"

  def __init__(self, url, user=None, password=None, labels={}, level="INFO", host=True, filename=True, python_version=True):
    self.password = password
    self.user = user
    self.url = url
    self.labels = labels
    self.level = level
    self.host = host
    self.filename = filename
    self.python_version = python_version

  def __error_handler(self, response):
    error = json.loads(response.content.decode("utf-8")).get("message")
    raise ConnectionError(f"Request error: {error}")

  def __basic_auth_log_post(self, payload):
    url = self.url + self.push_endpoint
    return requests.post(
      url,
      data=json.dumps(payload),
      headers={"Content-Type": "application/json"},
      auth=requests.auth.HTTPBasicAuth(self.user, self.password)
    )

  def __no_auth_log_post(self, payload):
    url = self.url + self.push_endpoint
    return requests.post(
      url,
      data=json.dumps(payload),
      headers={"Content-Type": "application/json"}
    )

  def log(self, data, labels=False, level=False):
    timestring = datetime.datetime.now().replace(microsecond=0).isoformat()
    
    if not level:
      level = self.level
    if not labels:
      labels = self.labels
    if self.host:
      labels["host"] = socket.gethostname()
    if self.filename:
      fname = sys.argv[0]
      if fname[:2] == "./":
        fname = fname[2:]
      labels["filename"] = fname
    if self.python_version:
      labels["python_version"] = sys.version.split(' ')[0]

    data = f"{timestring} [{level}]: {data}"
    payload = {"streams": [{ "stream": labels, "values": [ [ time.time_ns(), data ] ] }]}

    if self.user and self.password:
      response = self.__basic_auth_log_post(payload)
    else:
      response = self.__no_auth_log_post(payload)

    if response.status_code >= 300:
      error = json.loads(response.content.decode("utf-8")).get("error")
      self.__error_handler(response)
    return(response.status_code)

  def __basic_auth_query_get(self, payload):
    url = self.url + self.query_endpoint
    return requests.get(
      url,
      params=payload,
      headers={"Content-Type": "application/json"},
      auth=requests.auth.HTTPBasicAuth(self.user, self.password)
    )

  def query(self, query, limit, time=None, direction=None):
    payload = {}
    payload["query"] = query
    payload["limit"] = str(limit)
    if time:
      payload["time"] = time
    if direction:
      payload["direction"] = direction
    
    response = self.__basic_auth_query_get(payload)
    if response.status_code >= 300:
      self.__error_handler(response)
    return(json.loads(response.content.decode("utf-8")).get("data"))