import loki_driver, creds

url = "https://logs-prod-eu-west-0.grafana.net"

username = creds.username
password = creds.password

driver = loki_driver.driver(url, username, password)


driver.log("This is a log line")

driver.log("This is a warning", level="WARN")

try:
  "string".forceError
except Exception as e:
  driver.log(e, level="CRIT")
